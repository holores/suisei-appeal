import Link from 'next/link';

export default function Footer() {
	return (
		<div className="flex h-[4.5rem] w-full flex-col items-center justify-center px-8 py-4">
			<div className="flex items-center gap-2 text-center">
				<a href="https://docs.suisei.app">Documentation</a>
				<span>|</span>
				<a href="https://bitbucket.org/holores/workspace/projects/SUI">
					Source Code
				</a>
				<span>|</span>
				<p className="cursor-not-allowed">Dashboard</p>
			</div>
			<div className="flex items-center gap-2 text-center">
				<Link href="/terms" passHref>
					Terms of Use
				</Link>
				<span>|</span>
				<Link href="/privacy" passHref>
					Privacy Policy
				</Link>
			</div>
		</div>
	);
}
