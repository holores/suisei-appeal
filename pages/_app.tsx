import '../styles/globals.css';
import type { AppProps } from 'next/app';
import Head from 'next/head';

export default function MyApp({ Component, pageProps }: AppProps) {
	return (
		<>
			<Head>
				<title>Suisei&apos;s Mic</title>
			</Head>
			<Component {...pageProps} />
			<script
				defer
				src="https://static.cloudflareinsights.com/beacon.min.js"
				data-cf-beacon='{"token": "6795c7c525804b659c64ec6aa046c465"}'
			/>
		</>
	);
}
