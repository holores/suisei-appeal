// import NavBar from '../components/Navbar';
import Hero from '../components/Hero';
import Footer from '../components/Footer';

export default function HomePage() {
	return (
		<div className="min-w-screen max-w-screen flex min-h-screen flex-col">
			<Hero />
			<Footer />
		</div>
	);
}
